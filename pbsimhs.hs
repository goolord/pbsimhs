charizard :: Pokemon
charizard = Pokemon "Charizard" Fire Flying 

data Pokemon = Pokemon { getName:: String, getType1:: Type, getType2 :: Type } deriving (Show)
type Name = String 
data Type = Bug | Dragon | Ice | Fighting | Fire | Flying | Grass | Ghost | Ground | Electric | Normal | Poison | Psychic | Rock | Water deriving (Show)

charizardBaseStats :: [Float]
charizardBaseStats = [78, 84, 78, 85, 85, 100]
charizardEffortValues :: [Float]
charizardEffortValues = [252, 252, 252, 252, 252, 252]
charizardStats :: [(Float, Float)]
charizardStats = (zip charizardBaseStats charizardEffortValues)

stat:: Float -> Float -> Int
hp:: Float -> Float -> Int
statTuple = (\(x,y) -> stat x y)
hpTuple = (\(x,y) -> hp x y)

--stat formulas (generation agnostic)

stat x y = floor ( ( ( ( 31 + 2 * x + ( y / 4 ) ) * 100 / 100 ) + 5 ) * 1 )
hp x y = floor ( ( ( 31 + 2 * x + ( y / 4 ) ) * 100 / 100 ) + 10 + 100 )

--stats assuming max ev and ivs

charizardStatHp = hpTuple (charizardStats !! 0)
charizardStatAtk = statTuple (charizardStats !! 1)
charizardStatDef = statTuple (charizardStats !! 2)
charizardStatSpAtk = statTuple (charizardStats !! 3)
charizardStatSpDef = statTuple (charizardStats !! 4)
charizardStatSpd = statTuple (charizardStats !! 5)

--movelist

fireBlast = ("Fire Blast", "Fire", "Special", 120, 0.85, 8)
earthquake = ("Earthquake", "Ground", "Physical", 100, 1.0, 16)
swordsDance = ("Swords Dance", "Normal", "Non-Damaging", 2, "atk", "self")
hyperBeam = ("Hyper Beam", "Fire", "Special", 120, 0.85, 8)

--damage formula
--modifier = targets(.75 if > 1) * weather * badge(gen2) * critical(gen2+) * random(gen2+) * burn * other

damage :: Float -> Float -> Float -> Float -> Int
damage p a d m = floor ( ( ( ( ( ( 2 * 100 ) / 5 ) + 2 ) * p * a / d ) / 50 ) * m )
